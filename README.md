# Wzorzec Adapter

## 1. Zastosowanie
Wzorzec używa się w celu dostosowania istniejącego obiektu do potrzeb aplikacji. Wyobraźmy sobie sytuację, gdy mamy kilka takich samych czujników, które mogą być obsługiwany zarówno poprzez SPI jak i I2C i chcemy używać dla nich identycznego API wysokopoziomowego. Należy wówczas stworzyć tablicę obiektów, które będą posiadały uchwyt na używany hardware, funkcje komunikacyjne oraz adres.

## 2. Przykład użycia

Tablica obiektów:
```c
accelerometer_object sensors_array[NUMBER_OF_SENSORS] =
{
	{
		  i2c_read_function,
		  i2c_write_function,
		  (void*)&hi2c1,
		  FIRST_SENSOR_ADDRESS
	},
	{
		  spi_read_function,
		  spi_write_function,
		  (void*)&hspi2,
		  SECOND_SENSOR_ADDRESS
	}
};
```

Wywołania funkcji dla wybranego czujnika:
```c
read_sensor_register(SENSOR_1, 0x01);
```

Wybór konkretnego czujnika:
```c
uint8_t read_sensor_register(uint8_t sensor_number, uint8_t register_number)
{
	  assert(sensor_number < NUMBER_OF_SENSORS);
	  assert(register_number < MAX_REGISTER_NUMBER);

	  return sensors_array[sensor_number].read_callback_pointer(sensors_array[sensor_number].hal_hardware_pointer, sensors_array[sensor_number].device_address, register_number);
}
```

## 3. Przebieg programu:
Program cyklicznie odczytuje i zapisuje wartości do dwóch różnych czujników

```c
	  HAL_Delay(1000);
	  read_sensor_register(SENSOR_1, 0x01);
	  HAL_Delay(1000);
	  read_sensor_register(SENSOR_2, 0x01);
	  HAL_Delay(1000);
	  write_sensor_register(SENSOR_1, 0x03, 55);
	  HAL_Delay(1000);
	  write_sensor_register(SENSOR_2, 0x03, 55);
```

