/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <assert.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

  typedef uint8_t (*read_register_callback_t)(void *, uint8_t, uint8_t);
  typedef void (*write_register_callback_t)(void *, uint8_t, uint8_t, uint8_t);

  typedef struct
  {
	  read_register_callback_t read_callback_pointer;
	  write_register_callback_t write_callback_pointer;
	  void * hal_hardware_pointer;
	  uint8_t device_address;
  }accelerometer_object;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define MAX_REGISTER_NUMBER (255)

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_SPI2_Init(void);
/* USER CODE BEGIN PFP */

uint8_t i2c_read_function(void * hal_i2c_handler_arg, uint8_t device_address, uint8_t register_number);
void i2c_write_function(void * hal_i2c_handler_arg, uint8_t device_address, uint8_t register_number, uint8_t value_to_write);
uint8_t spi_read_function(void * hal_spi_handler_arg, uint8_t device_address, uint8_t register_number);
void spi_write_function(void * hal_spi_handler_arg, uint8_t device_address, uint8_t register_number, uint8_t value_to_write);
uint8_t read_sensor_register(uint8_t sensor_number, uint8_t register_number);
void write_sensor_register(uint8_t sensor_number, uint8_t register_number, uint8_t value_to_write);

#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)

PUTCHAR_PROTOTYPE
{
	HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, HAL_MAX_DELAY);
	return ch;
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

accelerometer_object sensors_array[NUMBER_OF_SENSORS] =
{
	{
		  i2c_read_function,
		  i2c_write_function,
		  (void*)&hi2c1,
		  FIRST_SENSOR_ADDRESS
	},
	{
		  spi_read_function,
		  spi_write_function,
		  (void*)&hspi2,
		  SECOND_SENSOR_ADDRESS
	}
};

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_SPI2_Init();
  /* USER CODE BEGIN 2 */

  printf("\e[1;1H\e[2J");
  printf("Adapter patern example started!\r\n");

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  HAL_Delay(1000);
	  read_sensor_register(SENSOR_1, 0x01);
	  HAL_Delay(1000);
	  read_sensor_register(SENSOR_2, 0x01);
	  HAL_Delay(1000);
	  write_sensor_register(SENSOR_1, 0x03, 55);
	  HAL_Delay(1000);
	  write_sensor_register(SENSOR_2, 0x03, 55);

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

uint8_t read_sensor_register(uint8_t sensor_number, uint8_t register_number)
{
	  assert(sensor_number < NUMBER_OF_SENSORS);
	  assert(register_number < MAX_REGISTER_NUMBER);

	  return sensors_array[sensor_number].read_callback_pointer(sensors_array[sensor_number].hal_hardware_pointer, sensors_array[sensor_number].device_address, register_number);
}

void write_sensor_register(uint8_t sensor_number, uint8_t register_number, uint8_t value_to_write)
{
	  assert(sensor_number < NUMBER_OF_SENSORS);
	  assert(register_number < MAX_REGISTER_NUMBER);

	  sensors_array[sensor_number].write_callback_pointer(sensors_array[sensor_number].hal_hardware_pointer, sensors_array[sensor_number].device_address, register_number, value_to_write);
}

uint8_t i2c_read_function(void * hal_i2c_handler_arg, uint8_t device_address, uint8_t register_number)
{
	assert(hal_i2c_handler_arg != NULL);

	I2C_HandleTypeDef * hal_i2c_handler = hal_i2c_handler_arg;
	(void)hal_i2c_handler;

	//read value from i2c, for example with HAL_I2C_Mem_Read, nothing is on the line so delay is just 1
	uint8_t receive_buffer = 0;
	HAL_I2C_Mem_Read(hal_i2c_handler, device_address, register_number, 1, &receive_buffer, 1, 1);

	printf("Value from I2C readed succesfully!\r\n");
	return 1;
}

void i2c_write_function(void * hal_i2c_handler_arg, uint8_t device_address, uint8_t register_number, uint8_t value_to_write)
{
	assert(hal_i2c_handler_arg != NULL);

	I2C_HandleTypeDef * hal_i2c_handler = hal_i2c_handler_arg;
	(void)hal_i2c_handler;

	//write value to i2c
	HAL_I2C_Mem_Write(hal_i2c_handler, device_address, register_number, 1, &value_to_write, 1, 1);

	printf("Value %u using I2C writed succesfully!\r\n", value_to_write);
}

uint8_t spi_read_function(void * hal_spi_handler_arg, uint8_t device_address, uint8_t register_number)
{
	assert(hal_spi_handler_arg != NULL);

	SPI_HandleTypeDef * hal_spi_handler = hal_spi_handler_arg;
	(void)hal_spi_handler;

	//read value from i2c, for example with HAL_SPI_TransmitReceive, nothing is on the line so delay is just 1
	uint8_t tx_buffer[2] = {device_address, register_number};
	uint8_t rx_buffer[2] = {0};
	HAL_SPI_TransmitReceive(hal_spi_handler, &tx_buffer[0], &rx_buffer[0], 2, 1);

	printf("Value from SPI readed succesfully!\r\n");
	return 1;
}

void spi_write_function(void * hal_spi_handler_arg, uint8_t device_address, uint8_t register_number, uint8_t value_to_write)
{
	assert(hal_spi_handler_arg != NULL);

	SPI_HandleTypeDef * hal_spi_handler = hal_spi_handler_arg;
	(void)hal_spi_handler;

	//write value to spi
	uint8_t tx_buffer[3] = {device_address, register_number, value_to_write};
	uint8_t rx_buffer[3] = {0};
	HAL_SPI_TransmitReceive(hal_spi_handler, &tx_buffer[0], &rx_buffer[0], 3, 1);

	printf("Value %u using SPI writed succesfully!\r\n", value_to_write);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
